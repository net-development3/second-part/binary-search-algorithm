﻿using System;

namespace BinarySearchTask
{
    public static class ArrayExtension
    {
        public static int? BinarySearch(int[] source, int value)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int? result = null;
            int left = 0, right = source.Length - 1;

            while (left <= right)
            {
                int mid = left + ((right - left) / 2);

                if (source[mid] < value)
                {
                    left = mid + 1;
                }
                else if (source[mid] > value)
                {
                    right = mid - 1;
                }
                else if (source[mid] == value)
                {
                    result = mid;
                    break;
                }
            }

            return result;
        }
    }
}
